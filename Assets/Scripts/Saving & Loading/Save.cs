﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class Save : MonoBehaviour
{
	private GameController gController;
	private PlayerController pController;
	private ShopManager sManager;
	public GameObject player;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if (gameControllerObject != null)
		{
			gController = gameControllerObject.GetComponent<GameController>();
		}
		if (gController == null)
		{
			Debug.Log("'GameController' er ikke eksisterende");
		}

		GameObject playerControllerObject = GameObject.FindWithTag("Player");
		if (playerControllerObject != null)
		{
			pController = playerControllerObject.GetComponent<PlayerController>();
		}
		if (pController == null)
		{
			Debug.Log("'PlayerController' er ikke eksisterende");
		}

		GameObject shopManagerObject = GameObject.FindWithTag("ShopManager");
		if (shopManagerObject != null)
		{
			sManager = shopManagerObject.GetComponent<ShopManager>();
		}
		if (sManager == null)
		{
			Debug.Log("'shopManager' er ikke eksisterende");
		}
	}


	public void SaveGame()
	{

		if (!Directory.Exists("Saves"))
		{
			Directory.CreateDirectory("Saves");
		}

		ES2.Save(player.transform.position, "Saves/playerPosition.gejr");

		ES2.Save(SceneManager.GetActiveScene().name, "Saves/sceneID.gejr");
		ES2.Save(gController.asteroidCount, "Saves/asteroidCount.gejr");
		ES2.Save(gController.waveCount, "Saves/waveCount.gejr");
		ES2.Save(gController.score, "Saves/score.gejr");
		ES2.Save(gController.coins, "Saves/coins.gejr");
		ES2.Save(pController.tilt, "Saves/tilt.gejr");
		ES2.Save(pController.speed, "Saves/speed.gejr");
		ES2.Save(pController.shooting.fireRate, "Saves/fireRate.gejr");
		ES2.Save(pController.shooting.doubleShot, "Saves/doubleShot.gejr");
		ES2.Save(sManager.upgradeShipSpeed, "Saves/upgradeShipSpeed.gejr");
		ES2.Save(sManager.upgradeShotSpeed, "Saves/upgradeShotSpeed.gejr");
		ES2.Save(sManager.upgradeMoreShots, "Saves/upgradeMoreShots.gejr");
	}
}