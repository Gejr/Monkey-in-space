﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Load : MonoBehaviour
{
	private GameController gController;
	private PlayerController pController;
	private ShopManager sManager;
	public GameObject player;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if (gameControllerObject != null)
		{
			gController = gameControllerObject.GetComponent<GameController>();
		}
		if (gController == null)
		{
			Debug.Log("'GameController' er ikke eksisterende");
		}

		GameObject playerControllerObject = GameObject.FindWithTag("Player");
		if (playerControllerObject != null)
		{
			pController = playerControllerObject.GetComponent<PlayerController>();
		}
		if (pController == null)
		{
			Debug.Log("'PlayerController' er ikke eksisterende");
		}

		GameObject shopManagerObject = GameObject.FindWithTag("ShopManager");
		if (shopManagerObject != null)
		{
			sManager = shopManagerObject.GetComponent<ShopManager>();
		}
		if (sManager == null)
		{
			Debug.Log("'shopManager' er ikke eksisterende");
		}

		LoadGame();
	}

	public void LoadGame()
	{
		player.transform.position = ES2.Load<Vector3>("Saves/playerPosition.gejr");

		gController.asteroidCount = ES2.Load<int>("Saves/asteroidCount.gejr");
		gController.waveCount = ES2.Load<int>("Saves/waveCount.gejr");
		gController.score = ES2.Load<int>("Saves/score.gejr");
		gController.coins = ES2.Load<int>("Saves/coins.gejr");
		//Application.loadedLevel = ES2.Load<int>("sceneID");
		pController.tilt = ES2.Load<float>("Saves/tilt.gejr");
		pController.speed = ES2.Load<float>("Saves/speed.gejr");
		pController.shooting.fireRate = ES2.Load<float>("Saves/fireRate.gejr");
		pController.shooting.doubleShot = ES2.Load<bool>("Saves/doubleShot.gejr");
		sManager.upgradeShipSpeed = ES2.Load<int>("Saves/upgradeShipSpeed.gejr");
		sManager.upgradeShotSpeed = ES2.Load<int>("Saves/upgradeShotSpeed.gejr");
		sManager.upgradeMoreShots = ES2.Load<int>("Saves/upgradeMoreShots.gejr");
	}
}
