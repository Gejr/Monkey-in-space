﻿/*
 * @summary			Spaceship Controller
 * @description		Spaceship arcade controller. v1.0 able to move in the X & Y axises. v2.0 able to shoot.
 * @version			3.1.0
 * @file				PlayerController.cs
 * @author			Malte Gejr 
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright			Copyright (C) 2016 - 2017 Malte Gejr
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class PlayerController : MonoBehaviour
{

	private Rigidbody rb;
	private AudioSource asource;

	public float speed;
	public float tilt;

	public TouchPad touchPad;

	//Boundary menu tab
	public Boundary boundary;

	//Shooting menu tab
	public Shooting shooting;

	private ShopManager shopManager;

	void Start()
	{

		rb = GetComponent<Rigidbody>();
		asource = GetComponent<AudioSource>();


		GameObject shopManagerObject = GameObject.FindWithTag("ShopManager");
		if (shopManagerObject != null)
		{
			shopManager = shopManagerObject.GetComponent<ShopManager>();
		}
		if (shopManager == null)
		{
			Debug.Log("'shopManager' er ikke eksisterende");
		}
	}

	void Update()
	{
		if (Input.GetButton("Jump") && Time.time > shooting.nextFire)
		{
			shooting.nextFire = Time.time + shooting.fireRate;
			if (shooting.doubleShot != true)
			{
				GameObject cloneleft = Instantiate(shooting.Shot, shooting.ShotSpawnLeft.position, shooting.ShotSpawnLeft.rotation) as GameObject;
			}
			else
			{
				GameObject cloneleft = Instantiate(shooting.Shot, shooting.ShotSpawnLeft.position, shooting.ShotSpawnLeft.rotation) as GameObject;
				GameObject cloneright = Instantiate(shooting.Shot, shooting.ShotSpawnRight.position, shooting.ShotSpawnRight.rotation) as GameObject;
			}
			asource.Play();
		}

	}

	void FixedUpdate()
	{
		//KEYBOARD CONTROL

		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		Vector3 movement = new Vector3(moveVertical, moveHorizontal, 0.0f);

		// PHONE TILT CONTROL

		//Vector3 acceleration = Input.acceleration;
		//Vector3 movement = new Vector3(acceleration.x, 0.0f, acceleration.y);

		// PHONE TOUCH INPUT CONTROL

		//Vector2 direction = touchPad.GetDirection();
		//Vector3 movement = new Vector3(direction.x, direction.y, 0.0f);

		//sætter din hastighed
		rb.velocity = (movement * speed);

		//Laver grænserne
        
		Vector3 bounds = new Vector3((Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax)), (Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax)), 0.0f);

		//Gør at du ikke kan komme forbi grænsen
		rb.position = bounds;

		// 2D Vertical tilt
		//rb.rotation = Quaternion.Euler(0.0f, 0.0f, (rb.velocity.x * -tilt));

		// 3D Vertical tilt
		rb.rotation = Quaternion.Euler(0.0f, (rb.velocity.x * -tilt), 0.0f);

		// Horizontal tilt
		// rb.rotation = Quaternion.Euler((rb.velocity.x * -tilt), 0.0f, 0.0f);

	}
}

[System.Serializable]
public class Shooting
{
	//Skyder variabler
	public GameObject Shot;
	public Transform ShotSpawnLeft;
	public Transform ShotSpawnRight;
	public float fireRate = 0.5f;
	public float nextFire = 0.0f;

	public bool doubleShot = false;
}

[System.Serializable]
public class Boundary
{

	//Grænse variabler
	public float xMax;
	public float xMin;
	public float yMax;
	public float yMin;
}