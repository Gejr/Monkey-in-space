﻿/*
 * @summary			Game Controller
 * @description		xxxx
 * @version			4.3.1
 * @file				GameController.cs
 * @author			Malte Gejr 
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright			Copyright (C) 2016 - 2017 Malte Gejr
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class GameController : MonoBehaviour
{

	public GameObject[] asteroids;
	public Vector3 spawnValues;
	public int asteroidCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	public int waveCount;
	public float shopWait = 1000000;

	public Text scoreText;
	public int score;

	public Text coinText;
	public int coins;

	public Text restartText;
	public GameObject restartPanel;
	public Button restartButton;
	private bool restart;

	public Text gameOverText;
	private bool gameOver;

	public Text livesText;

	public int SceneID;

	public GameObject shopPanel;

	public Save save;
	public Load load;


	void OnEnable()
	{

		restartButton.onClick.AddListener(delegate
		{
			OnRestartGamePress();
		});
	}

	void Start()
	{
		GameObject saveObject = GameObject.FindWithTag("SaveLoad");
		if (saveObject != null)
		{
			save = saveObject.GetComponent<Save>();
		}
		if (save == null)
		{
			Debug.Log("'Save' er ikke eksisterende");
		}

		GameObject loadObject = GameObject.FindWithTag("SaveLoad");
		if (loadObject != null)
		{
			load = loadObject.GetComponent<Load>();
		}
		if (load == null)
		{
			Debug.Log("'Load' er ikke eksisterende");
		}

		gameOver = false;
		restart = false;

		livesText.text = " Lives: 3";
		restartText.text = "";
		gameOverText.text = "";

		UpdateScore();
		UpdateCoins();
		StartCoroutine(SpawnWaves());
	}

	void Update()
	{
		UpdateScore();
		UpdateCoins();

		if (restart == true)
		{
			if (Input.GetKeyDown(KeyCode.R))
			{
				Application.LoadLevel(Application.loadedLevel);
			}
		}
	}

	public IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds(startWait);
		while (true) //infinte wave loop, alternativt lav en "public int waveCount" og erstat "true" med "int i = 0; i < waveCount; i++"
		{
			for (int i = 0; i < asteroidCount; i++)
			{

				GameObject asteroid = asteroids[Random.Range(0, asteroids.Length)];

				Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate(asteroid, spawnPosition, spawnRotation);
				yield return new WaitForSeconds(spawnWait);
			}

			waveCount++;
			if (gameOver == true)
			{
				restartText.text = "Press 'R' to restart";
				restart = true;
				break;
			}
			save.SaveGame();
			yield return new WaitForSeconds(waveWait);

			if (waveCount % 5 == 0)
			{
				//yield return new WaitForSeconds(shopWait);
				shopPanel.SetActive(true);
				asteroidCount = asteroidCount + 5;
			}
			if (shopPanel.activeInHierarchy == true)
			{
				if (Time.timeScale == 1)
				{
					Time.timeScale = 0;
				}
				else
				{
					Time.timeScale = 1;
				}
			}


		}
	}

	public void AddScore(int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore();
	}

	public void AddCoins(int newCoinValue)
	{
		coins += newCoinValue;
		UpdateCoins();
	}

	public void RemoveCoins(int newCoinValue)
	{
		coins -= newCoinValue;
		UpdateCoins();
	}

	public void UpdateScore()
	{
		scoreText.text = " Score: " + score;
	}

	public void UpdateCoins()
	{
		coinText.text = " Coins: " + coins;
	}

	public void GameOver()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;
		restartPanel.SetActive(true);
	}

	public void OnRestartGamePress()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
}
