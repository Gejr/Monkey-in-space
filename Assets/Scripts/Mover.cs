﻿/*
 * @summary			Spaceship Controller, bullet mover
 * @description		Spaceship shot mover. Moves the bullet
 * @version			1.0.1
 * @file				Mover.cs
 * @author			Malte Gejr 
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017 Malte Gejr
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */
 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{

	private Rigidbody rb;

	public float speed;

	void Start()
	{
		rb = GetComponent<Rigidbody>();

		rb.velocity = transform.up * speed;
	}
}
