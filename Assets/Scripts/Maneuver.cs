﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maneuver : MonoBehaviour
{
	public float dodge;
	public float smoothing;
	public float tilt;
	public Vector2 startWait;
	public Vector2 maneuverTime;
	public Vector2 maneuverWait;
	public Boundary boundary;

	private float currentSpeed;
	private float targetManeuver;
	private Rigidbody rb;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
		currentSpeed = rb.velocity.y; //skal måske være rb.velocity.z
		StartCoroutine(Evade());
	}

	IEnumerator Evade()
	{
		yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

		while (true)
		{
			targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);
			yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
			targetManeuver = 0;
			yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
		}
	}

	void FixedUpdate()
	{
		float newManeuver = Mathf.MoveTowards(rb.velocity.x, targetManeuver, Time.deltaTime * smoothing);
		rb.velocity = new Vector3(newManeuver, currentSpeed, 0.0f); //skal måske være (newManeuver, 0.0f, currentSpeed);
		rb.position = new Vector3((Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax)), (Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax)), 0.0f);

		// 2D Vertical tilt
		//rb.rotation = Quaternion.Euler(0.0f, 0.0f, (rb.velocity.x * -tilt));

		// 3D Vertical tilt
		rb.rotation = Quaternion.Euler(0.0f, (rb.velocity.x * -tilt), 0.0f);

		// Horizontal tilt
		// rb.rotation = Quaternion.Euler((rb.velocity.x * -tilt), 0.0f, 0.0f);
	}
}
