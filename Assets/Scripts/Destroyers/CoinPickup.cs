﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickup : MonoBehaviour
{
	public GameObject coinPickup;

	private GameController gameController;
	public int coinValue;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if (gameController == null)
		{
			Debug.Log("'GameController' er ikke eksisterende");
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Boundary")
		{
			return;
		}

		if (other.tag == "Player")
		{
			Instantiate(coinPickup, other.transform.position, other.transform.rotation);
			Destroy(gameObject);
			gameController.AddCoins(coinValue);
		}

		if (other.tag == "Coin")
		{
			return;
		}
	}
}
