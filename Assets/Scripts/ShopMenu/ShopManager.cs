﻿/*
 * @summary			Shop Manager
 * @description		xxxx
 * @version			2.2.1
 * @file				ShopManager.cs
 * @author			Malte Gejr 
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright			Copyright (C) 2016 - 2017 Malte Gejr
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class ShopManager : MonoBehaviour
{

	public Button shipSpeedButton;
	public Button shotSpeedButton;
	public Button moreShotButton;
	public Button continueGameButton;
	public Button buyButton;
	public Image itemImage;
	public Text descText;
	public Text priceText;
	public Sprite Motor;
	public Sprite Bullet;
	public Sprite moreBullets;

	public GameObject shopPanel;

	private GameController gameController;
	private PlayerController playerController;

	public int upgradeShipSpeed;
	public int upgradeShotSpeed;
	public int upgradeMoreShots;

	private int chosen;

	void OnEnable()
	{
		shipSpeedButton.onClick.AddListener(delegate
		{
			OnShipSpeedPress();
		});
		shotSpeedButton.onClick.AddListener(delegate
		{
			OnShotSpeedPress();
		});
		moreShotButton.onClick.AddListener(delegate
		{
			OnMoreShotPress();
		});
		continueGameButton.onClick.AddListener(delegate
		{
			OnContinueGamePress();
		});
		buyButton.onClick.AddListener(delegate
		{
			OnBuyPress();
		});
	}

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if (gameController == null)
		{
			Debug.Log("'GameController' er ikke eksisterende");
		}

		GameObject playerControllerObject = GameObject.FindWithTag("Player");
		if (playerControllerObject != null)
		{
			playerController = playerControllerObject.GetComponent<PlayerController>();
		}
		if (playerController == null)
		{
			Debug.Log("'PlayerController' er ikke eksisterende");
		}

		upgradeShipSpeed = 0;
		upgradeShotSpeed = 0;
		upgradeMoreShots = 0;

		//reloader defualt valgte upgrade
		OnShipSpeedPress();

		chosen = 1;
	}

	void OnShipSpeedPress()
	{
		Debug.Log("OnShipSpeedPress");
		chosen = 1;
		checkUpgradeStateShipSpeed();
		itemImage.overrideSprite = Motor;
		descText.text = "Ayyyy Lmao Billy mays here, This sick motor is capable of flying faster than even sanic!!!!! plzz buy it now, only 9.99 $ ";
		if (upgradeShipSpeed == 0)
		{
			priceText.text = "Price: 10 $";
		}
		else if (upgradeShipSpeed == 1)
		{
			priceText.text = "Price: 20 $";
		}
		else if (upgradeShipSpeed == 2)
		{
			priceText.text = "Price: 30 $";
		}
		else if (upgradeShipSpeed == 3)
		{
			priceText.text = "Price: 40 $";
		}
		else if (upgradeShipSpeed == 4)
		{
			priceText.text = "Fully Upgraded";
		}
	}

	void OnShotSpeedPress()
	{
		Debug.Log("OnShotSpeedPress");
		chosen = 2;
		checkUpgradeStateShotSpeed();
		itemImage.overrideSprite = Bullet;
		descText.text = "Billy mays here agian, now with a new amazing offer, only for YOU! buy this amazing bullet upgrade, your bullet will fly faster than sanic!!!!";
		if (upgradeShotSpeed == 0)
		{
			priceText.text = "Price: 15 $";
		}
		else if (upgradeShotSpeed == 1)
		{
			priceText.text = "Price: 20 $";
		}
		else if (upgradeShotSpeed == 2)
		{
			priceText.text = "Price: 30 $";
		}
		else if (upgradeShotSpeed == 3)
		{
			priceText.text = "Price: 40 $";
		}
		else if (upgradeShotSpeed == 4)
		{
			priceText.text = "Fully Upgraded";
		}
	}

	void OnMoreShotPress()
	{
		Debug.Log("OnMoreShotPress");
		chosen = 3;
		checkUpgradeStateMoreShots();
		itemImage.overrideSprite = moreBullets;
		descText.text = "yooo buy this item and get a double chance for coins, you will also get double score!!! thats smart as shit yo! Billey mays recommend this shit";
		if (upgradeMoreShots == 0)
		{
			priceText.text = "Price: 50 $";
		}
		else if (upgradeMoreShots == 1)
		{
			priceText.text = "Fully Upgraded";
		}
	}

	void OnContinueGamePress()
	{
		Debug.Log("OnContinueGamePress");
		shopPanel.SetActive(false);
		if (Time.timeScale == 0)
		{
			Time.timeScale = 1;
		}
		else
		{
			Time.timeScale = 1;
		}
	}

	void OnBuyPress()
	{
		Debug.Log("OnBuyPress");
		if (chosen == 1)
		{
			Debug.Log("upgradeShipSpeed");


			if (upgradeShipSpeed == 0 && gameController.coins >= 10)
			{
				gameController.RemoveCoins(10);
				Debug.Log("RemoveCoins(10)");
				playerController.speed = 5f;
				upgradeShipSpeed++;
				OnShipSpeedPress();
				return;

			}

			if (upgradeShipSpeed == 1 && gameController.coins >= 20)
			{
				gameController.RemoveCoins(20);
				Debug.Log("RemoveCoins(20)");
				playerController.speed = 7f;
				upgradeShipSpeed++;
				OnShipSpeedPress();
				return;
			}

			if (upgradeShipSpeed == 2 && gameController.coins >= 30)
			{
				gameController.RemoveCoins(30);
				Debug.Log("RemoveCoins(30)");
				playerController.speed = 9f;
				upgradeShipSpeed++;
				OnShipSpeedPress();
				return;
			}

			if (upgradeShipSpeed == 3 && gameController.coins >= 40)
			{
				gameController.RemoveCoins(40);
				Debug.Log("RemoveCoins(40)");
				playerController.speed = 11f;
				buyButton.interactable = false;
				OnShipSpeedPress();
				return;
			}
			if (upgradeShipSpeed >= 4)
			{
				buyButton.interactable = false;
			}
		}

		if (chosen == 2)
		{
			Debug.Log("upgradeShotSpeed");

			if (upgradeShotSpeed == 0 && gameController.coins >= 15)
			{
				gameController.RemoveCoins(15);
				Debug.Log("RemoveCoins(15)");
				playerController.shooting.fireRate = 0.4f;
				upgradeShotSpeed++;
				OnShotSpeedPress();
				return;
			}

			if (upgradeShotSpeed == 1 && gameController.coins >= 20)
			{
				gameController.RemoveCoins(20);
				Debug.Log("RemoveCoins(20)");
				playerController.shooting.fireRate = 0.3f;
				upgradeShotSpeed++;
				OnShotSpeedPress();
				return;
			}

			if (upgradeShotSpeed == 2 && gameController.coins >= 30)
			{
				gameController.RemoveCoins(30);
				Debug.Log("RemoveCoins(30)");
				playerController.shooting.fireRate = 0.2f;
				upgradeShotSpeed++;
				OnShotSpeedPress();
				return;
			}

			if (upgradeShotSpeed == 3 && gameController.coins >= 40)
			{
				gameController.RemoveCoins(40);
				Debug.Log("RemoveCoins(40)");
				playerController.shooting.fireRate = 0.1f;
				buyButton.interactable = false;
				OnShotSpeedPress();
				upgradeShotSpeed++;
				return;
			}
			if (upgradeMoreShots >= 4)
			{
				buyButton.interactable = false;
			}
		}

		if (chosen == 3)
		{
			Debug.Log("upgradeMoreShots");

			if (upgradeMoreShots == 0 && gameController.coins >= 50)
			{
				gameController.RemoveCoins(50);
				Debug.Log("RemoveCoins(50)");
				playerController.shooting.doubleShot = true;
				buyButton.interactable = false;
				upgradeMoreShots++;
				OnMoreShotPress();
				return;
			}
			if (upgradeMoreShots >= 1)
			{
				buyButton.interactable = false;
			}
		}
	}

	void checkUpgradeStateShipSpeed()
	{
		if (upgradeShipSpeed == 4)
		{
			buyButton.interactable = false;
		}
		else
		{
			buyButton.interactable = true;
		}
	}

	void checkUpgradeStateShotSpeed()
	{
		if (upgradeShotSpeed == 4)
		{
			buyButton.interactable = false;
		}
		else
		{
			buyButton.interactable = true;
		}
	}

	void checkUpgradeStateMoreShots()
	{
		if (upgradeMoreShots == 1)
		{
			buyButton.interactable = false;
		}
		else
		{
			buyButton.interactable = true;
		}
	}
}
