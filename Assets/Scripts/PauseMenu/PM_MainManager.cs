﻿/*
 * @summary			Pause Main menu UI
 * @description		Main menu with lot's of customizable things, includes new game, load game, options, exit game
 * @version			1.0.1
 * @file				PM_MenuManager.cs
 * @author			Malte Gejr 
 * @contact			www.mkkvk.dk/kontakt
 * 
 * @copyright	    Copyright (C) 2016 - 2017 Malte Gejr
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;
using TGT.Menu;
using UnityEngine.Experimental.Director;

public class PM_MainManager : MonoBehaviour
{
	public Button continueGameButton;
	public Button settingsButton;
	public Button exitGameButton;
	public Button testButton;
	public Button test2Button;
	public Button menuButton;

	public GameObject mainmenuPanel;
	public GameObject optionsPanel;
	public GameObject sureExitPanel;
	public GameObject picturePanel;
	public GameObject catchManager;

	private GameController gameController;
	private PlayerController playerController;
	private ShopManager shopManager;
	private Save save;
	private Load load;

	void OnEnable()
	{

		continueGameButton.onClick.AddListener(delegate
		{
			OnContinueGamePress();
		});

		settingsButton.onClick.AddListener(delegate
		{
			OnSettingsPress();
		});

		exitGameButton.onClick.AddListener(delegate
		{
			OnExitGamePress();
		});
		testButton.onClick.AddListener(delegate
		{
			OnTestPress();	
		});
		test2Button.onClick.AddListener(delegate
		{
			OnTest2Press();	
		});

	}

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if (gameController == null)
		{
			Debug.Log("'GameController' er ikke eksisterende");
		}

		GameObject playerControllerObject = GameObject.FindWithTag("Player");
		if (playerControllerObject != null)
		{
			playerController = playerControllerObject.GetComponent<PlayerController>();
		}
		if (playerController == null)
		{
			Debug.Log("'PlayerController' er ikke eksisterende");
		}

		GameObject shopManagerObject = GameObject.FindWithTag("ShopManager");
		if (shopManagerObject != null)
		{
			shopManager = shopManagerObject.GetComponent<ShopManager>();
		}
		if (shopManager == null)
		{
			Debug.Log("'shopManager' er ikke eksisterende");
		}

		GameObject saveObject = GameObject.FindWithTag("SaveLoad");
		if (saveObject != null)
		{
			save = saveObject.GetComponent<Save>();
		}
		if (save == null)
		{
			Debug.Log("'Save' er ikke eksisterende");
		}

		GameObject loadObject = GameObject.FindWithTag("SaveLoad");
		if (loadObject != null)
		{
			load = loadObject.GetComponent<Load>();
		}
		if (load == null)
		{
			Debug.Log("'Load' er ikke eksisterende");
		}
	}

	public void OnContinueGamePress()
	{
		Debug.Log("OnContinueGamePress");
		mainmenuPanel.SetActive(false);
		picturePanel.SetActive(false);
		catchManager.SetActive(true);
		menuButton.interactable = true;

		if (Time.timeScale == 0)
		{
			Time.timeScale = 1;
		}
	}

	public void OnSettingsPress()
	{
		Debug.Log("OnSettingsPress");
		mainmenuPanel.SetActive(false);
		optionsPanel.SetActive(true);
	}

	public void OnExitGamePress()
	{
		Debug.Log("OnExitGamePress");
		sureExitPanel.SetActive(true);
		mainmenuPanel.SetActive(false);
	}

	public void OnTestPress()
	{
		Debug.Log("OnTestPress");
		save.SaveGame();
	}

	public void OnTest2Press()
	{
		Debug.Log("OnTest2Press");
		load.LoadGame();
		gameController.UpdateScore();
		gameController.UpdateCoins();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			mainmenuPanel.SetActive(false);
			picturePanel.SetActive(false);
			catchManager.SetActive(true);
			menuButton.interactable = true;

			if (Time.timeScale == 0)
			{
				Time.timeScale = 1;
			}
		}
	}
}