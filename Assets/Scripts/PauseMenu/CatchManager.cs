﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TGT.Menu;
using UnityEngine.UI;

public class CatchManager : MonoBehaviour
{
	public GameObject mainmenuPanel;
	public GameObject picturePanel;
	public GameObject catchManager;

	public Button menuButton;

	void OnEnable()
	{
		menuButton.onClick.AddListener(delegate
		{
			OnMenuButtonPress();
		});
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			mainmenuPanel.SetActive(true);
			picturePanel.SetActive(true);
			catchManager.SetActive(false);
			menuButton.interactable = false;
		}
	}

	void OnMenuButtonPress()
	{
		Debug.Log("OnGUIMenuButtonPress");
		mainmenuPanel.SetActive(true);
		picturePanel.SetActive(true);
		catchManager.SetActive(false);
		menuButton.interactable = false;

		if (Time.timeScale == 1)
		{
			Time.timeScale = 0;
		}
	}
}
