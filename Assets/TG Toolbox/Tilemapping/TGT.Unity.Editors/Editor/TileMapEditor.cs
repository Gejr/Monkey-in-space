/*
 * @summary			Tilemap
 * @description		Creates a Tilemap in the screen view, made for 2D games.
 * @version			1.0.1
 * @file			TileMapEditor.cs
 * @author			Malte Gejr 
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017 Malte Gejr
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */
 
namespace TGT.Unity.Editors.Editor
{
	using System;
	using UnityEditor;
	using UnityEngine;
	using TGT.TileMapping.Unity;

	//Laver en Custom Editor til TileMap.cs
	[CustomEditor(typeof(TileMap))]
	public class TileMapEditor : Editor
	{
		//Holder positionen af musens klik location
		private Vector3 mouseHitPos;
		
		//Lader Editor ordne en Event i "Scene View"
		private void OnSceneGUI()
		{
			//Hvis UpdateHitPosition er true, så skal vi opdatere "Screen View" så vi kan se markøren i Real Time
			if (this.UpdateHitPosition())
			{
				SceneView.RepaintAll();
			}

			//Udregner lokalitionen af markøren ud fra muse markørens lokalition
			this.RecalculateMarkerPosition();
			
			//Finder den nuværende Event
			Event current = Event.current;

			// Hvis musen er holdt hen over det layer, så skal der gives tilladelse til "drawing actions"
			if (this.IsMouseOnLayer())
			{
				//Hvis muse knappen holdes nede eller der trækkes med muse kanppen nede.
				if (current.type == EventType.MouseDown || current.type == EventType.MouseDrag)
				{
					//Hvis højre museknap trykkes ned skal der slettes celler
					if (current.button == 1)
					{
						this.Erase();
						current.Use();
					}
					//Hvis venstre museknap trykkes ned skal der tegnes celler
					else if (current.button == 0)
					{
						this.Draw();
						current.Use();
					}
				}
			}
			
			//Tegner et UI tip i "scene view" der viser information om hvordan man tegner og sletter
			Handles.BeginGUI();
			GUI.Label(new Rect(10, Screen.height - 90, 100, 100), "LMB: Draw");
			GUI.Label(new Rect(10, Screen.height - 105, 100, 100), "RMB: Erase");
			Handles.EndGUI();
		}

		//Når GameObject'et er valgt, så sæt det nuværende værktøj til "View Tool"
		private void OnEnable()
		{
			Tools.current = Tool.View;
			Tools.viewTool = ViewTool.FPS;
		}

		//Tegner en block ved det forregnede mouseHitPos
		private void Draw()
		{
			//Får reference til TileMap.cs
			var map = (TileMap) this.target;
			
			//Regner positionen af musen over "tile layer"
			var tilePos = this.GetTilePositionFromMouseLocation();
			
			//Med tilePos'es position, se om der allerede findes en tile på denne position
			var cube = GameObject.Find(string.Format("Tile_{0}_{1}", tilePos.x, tilePos.y));

			//Hvis der allerede er en tile tilsted pg det ikke er et "child" af GameObject'et så kan vi bare afslutte
			if (cube != null && cube.transform.parent != map.transform)
			{
				//Debug.Log("Ingen tile blevet lavet da der allerede findes en");
				return;
			}

			//Hvis der ikke findes en tile, så laver vi en
			if (cube == null)
			{
				//Debug.Log("En tile blev lavet");
				cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			}

			//Sætter "tile(cube)"'s position på tile map
			var tilePositionInLocalSpace = new Vector3((tilePos.x * map.TileWidth) + (map.TileWidth / 2), (tilePos.y * map.TileHeight) + (map.TileHeight / 2));
			cube.transform.position = map.transform.position + tilePositionInLocalSpace;

			//Vi skalere "cube"'en så den passer til "tile size" defineret af TileMap.TileWidth og TileMap.TileHeight felterne
			cube.transform.localScale = new Vector3(map.TileWidth, map.TileHeight, 1);

			//Sætter "cube"'ens "parent" til game object'et for orgaisering
			cube.transform.parent = map.transform;

			//Giver "cube"'en et navn der representere dens lokalition på tile mappet
			cube.name = string.Format("Tile_{0}_{1}", tilePos.x, tilePos.y);
		}

		//Sletter en celle ved det forregnede mouseHitPos
		private void Erase()
		{
			//Laver en reference til TileMap.cs
			var map = (TileMap) this.target;
			
			//Regner positionen af musen over "tile layer"
			var tilePos = this.GetTilePositionFromMouseLocation();
			
			//Med tilePos'es position, se om der allerede findes en tile på denne position
			var cube = GameObject.Find(string.Format("Tile_{0}_{1}", tilePos.x, tilePos.y));

			//Hvis en cube er fundet på positionen, med det rigtige navn, og den rigtige "parent" så skal den slettes med det samme
			if (cube != null && cube.transform.parent == map.transform)
			{
				//Debug.Log("En tile blev slettet");
				UnityEngine.Object.DestroyImmediate(cube);
			}
			
			//Hvis der ikke findes en cube på denne position, så skal vi bare afslutte
			if (cube == null)
			{
				//Debug.Log("Ingen ting blev slettet");
				return;
			}
		}

		//Regner koordinaterne(Columns og Rows) ud fra musemarkørens position
		private Vector2 GetTilePositionFromMouseLocation()
		{
			//Laver en reference til TileMap.cs
			var map = (TileMap) this.target;
			
			//Udregner lokalition (Columns & Rows) ud fra musens position
			var pos = new Vector3(this.mouseHitPos.x / map.TileWidth, this.mouseHitPos.y / map.TileHeight, map.transform.position.z);
			
			//Runder tallet op eller ned, til det nærmeste Hele tal, så der ikke bliver placeret tiles halvt inde i griddet
			pos = new Vector3((int) Math.Round(pos.x, 5, MidpointRounding.ToEven), (int) Math.Round(pos.y, 5, MidpointRounding.ToEven), 0);

			//Laver et tjek på at Rows og Columns er en grænse på hvor de kan blive placeret
			var col = (int) pos.x;
			var row = (int) pos.y;
			
			if (row < 0)
			{
				row = 0;
			}

			if (row > map.Rows - 1)
			{
				row = map.Rows - 1;
			}

			if (col < 0)
			{
				col = 0;
			}

			if (col > map.Columns - 1)
			{
				col = map.Columns - 1;
			}

			//Giver Columns og Rows værdier
			return new Vector2(col, row);
		}

		//Giver True eller False alt efter om musen holdes hen over tile mappet
		private bool IsMouseOnLayer()
		{
			//Laver en reference til TileMap.cs
			var map = (TileMap) this.target;
			
			//Giver True eller False alt efter om musen holdes hen over tile mappet
			return this.mouseHitPos.x > 0 && this.mouseHitPos.x < (map.Columns * map.TileWidth) &&
			this.mouseHitPos.y > 0 && this.mouseHitPos.y < (map.Rows * map.TileHeight);
		}

		//Gen udregner position ud fra markøren, som blev udregnet ud fra musen
		private void RecalculateMarkerPosition()
		{
			//Laver en reference til TileMap.cs
			var map = (TileMap) this.target;
			
			//Regner positionen af musen over "tile layer"
			var tilepos = this.GetTilePositionFromMouseLocation();
			
			//Gemmer "tilePos" i "world space"
			var pos = new Vector3(tilepos.x * map.TileWidth, tilepos.y * map.TileHeight, 0);

			//Sætter "TileMap.MarkerPosition" positionen
			map.MarkerPosition = map.transform.position + new Vector3(pos.x + (map.TileWidth / 2), pos.y + (map.TileHeight / 2), 0);
		}

		//Udregner positionen af musens position ud fra "local space" koordinaterne
		private bool UpdateHitPosition()
		{
			//Laver en reference til TileMap.cs
			var map = (TileMap) this.target;
			
			//Laver et "plane" object
			var p = new Plane(map.transform.TransformDirection(Vector3.forward), map.transform.position);
			
			//Som laver et raycast type, udfra den nuværende mousePosition
			var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
			
			//Gemmer hit information
			var hit = new Vector3();
			
			//Gemmer distancen til hit'et
			float dist;

			//Sender et Raycast ud som finder ud af hvornår den den rammer "plane" objectet
			if (p.Raycast(ray, out dist))
			{
				//Vi udregner hvorlangt der var til "plane" objektet
				hit = ray.origin + (ray.direction.normalized * dist);
			}

			//Konventere hit'et fra "world space" til "local space"
			var value = map.transform.InverseTransformPoint(hit);

			//Hvis værdien er noget andet end den nuværende mousePosition
			//Så sætter vi værdien til true, så den tror det var rigtigt
			if (value != this.mouseHitPos)
			{
				this.mouseHitPos = value;
				return true;
			}

			//Afslutter med false hvis hit testen fejlede
			return false;
		}
	}
}