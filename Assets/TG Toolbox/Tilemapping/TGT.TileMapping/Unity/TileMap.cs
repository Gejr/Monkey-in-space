﻿/*
 * @summary			Tilemap
 * @description		Creates a Tilemap in the screen view, made for 2D games.
 * @version			1.0.1
 * @file			TileMap.cs
 * @author			Malte Gejr 
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017 Malte Gejr
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */
 
namespace TGT.TileMapping.Unity
{
	using UnityEngine;

	public class TileMap : MonoBehaviour
	{
		public int Rows;
		public int Columns;

		public float TileWidth = 1;
		public float TileHeight = 1;
		
		// Denne variabel bruges kun som logic og er derfor gemt så man ikke kan se den i Inspector
		[HideInInspector]
		public Vector3 MarkerPosition;
		
		//Laver et TileMap, med de nedeunder defineret "Columns" og "Rows"
		public TileMap()
		{
			this.Columns = 20;
			this.Rows = 10;
		}
		
		//Bliver kun kaldt i Unity Editor
		private void OnDrawGizmosSelected()
		{
			//Gemmer Højde, bredde og position
			var mapWidth = this.Columns * this.TileWidth;
			var mapHeight = this.Rows * this.TileHeight;
			var position = this.transform.position;
			
			//Tegner Gizmos kant-linjer i Unity Editor
			Gizmos.color = Color.white;
			Gizmos.DrawLine(position, position + new Vector3(mapWidth, 0, 0));
			Gizmos.DrawLine(position, position + new Vector3(0, mapHeight, 0));
			Gizmos.DrawLine(position + new Vector3(mapWidth, 0, 0), position + new Vector3(mapWidth, mapHeight, 0));
			Gizmos.DrawLine(position + new Vector3(0, mapHeight, 0), position + new Vector3(mapWidth, mapHeight, 0));
			
			//Tegner tile celler
			Gizmos.color = Color.grey;
			for (float i = 1; i < this.Columns; i++)
			{
				Gizmos.DrawLine(position + new Vector3(i * this.TileWidth, 0, 0), position + new Vector3(i * this.TileWidth, mapHeight, 0));
			}
            
			for (float i = 1; i < this.Rows; i++)
			{
				Gizmos.DrawLine(position + new Vector3(0, i * this.TileHeight, 0), position + new Vector3(mapWidth, i * this.TileHeight, 0));
			}
			
			//Tegner outline på der hvor "tile"'en bliver placeret
			Gizmos.color = Color.red;    
			Gizmos.DrawWireCube(this.MarkerPosition, new Vector3(this.TileWidth, this.TileHeight, 1) * 1.1f);
		}
	}
}
