﻿/*
 * @summary			Raycasting
 * @description		Makes some raycast options easier to use and access.
 * @version			1.0.1
 * @file			StandardRaycast.cs
 * @author			Malte Gejr 
 * @contact			www.mkkvk.dk/kontakt
 * 
 * @copyright		Copyright (C) 2016 - 2017 Malte Gejr
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */

using UnityEngine;
using System.Collections;

public class StandardRaycast : MonoBehaviour
{

	public static string GetTagFromRaycast(Vector3 origin, Vector3 direction, float length)
	{
		string retVal = "";
		RaycastHit hitInfo;
		if (Physics.Raycast(origin, direction, out hitInfo, length))
		{
			retVal = hitInfo.collider.tag;
		}
		return retVal;
	}
}
